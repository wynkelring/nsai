package com.politechnika.nsai;

import com.politechnika.nsai.model.Participant;
import com.politechnika.nsai.repository.ParticipantRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DataConstructor {

    private final ParticipantRepository participantRepository;

    public DataConstructor(ParticipantRepository participantRepository) {
        this.participantRepository = participantRepository;
    }

    @PostConstruct
    private void addData() {
        Participant participant1 = new Participant();
        participant1.setEmail("email1@email.pl");
        participant1.setFirstName("Firstname1");
        participant1.setLastName("Lastname1");

        participantRepository.save(participant1);


        Participant participant2 = new Participant();
        participant2.setEmail("email2@email.pl");
        participant2.setFirstName("Firstname2");
        participant2.setLastName("Lastname2");

        participantRepository.save(participant2);


        Participant participant3 = new Participant();
        participant3.setEmail("email3@email.pl");
        participant3.setFirstName("Firstname3");
        participant3.setLastName("Lastname3");

        participantRepository.save(participant3);
    }
}
