package com.politechnika.nsai.repository;

import com.politechnika.nsai.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParticipantRepository extends JpaRepository<Participant, Long> {
    Participant findByEmail(String email);
    List<Participant> findByEmailIn(List<String> emails);

    boolean existsByEmail(String email);
}
