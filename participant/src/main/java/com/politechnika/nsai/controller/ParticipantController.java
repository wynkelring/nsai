package com.politechnika.nsai.controller;

import com.politechnika.nsai.model.Participant;
import com.politechnika.nsai.repository.ParticipantRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.util.Objects.isNull;

@RestController
@RequestMapping("/participants")
public class ParticipantController {

    private final ParticipantRepository participantRepository;

    public ParticipantController(ParticipantRepository participantRepository) {
        this.participantRepository = participantRepository;
    }

    @GetMapping
    public List<Participant> getAllParticipants() {
        return participantRepository.findAll();
    }


    @PostMapping
    public Participant saveParticipant(@RequestBody @Valid Participant participant) {
        //todo add validation if email exists (must be unique)
        return participantRepository.save(participant);
    }

    @GetMapping("/{participantId}")
    public ResponseEntity<Participant> getParticipantById(@PathVariable Long participantId) {
        return participantRepository.findById(participantId)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/email/{email}")
    public Participant getParticipantByEmail(@PathVariable String email) {
        return participantRepository.findByEmail(email);
    }

    @GetMapping("/emails")
    public List<Participant> getParticipantByEmail(@RequestParam(required = false) List<String> emails) {
        if (isNull(emails)) {
            return participantRepository.findAll();
        }
        return participantRepository.findByEmailIn(emails);
    }

    @DeleteMapping("/{participantId}")
    public ResponseEntity<?> deleteParticipant(@PathVariable Long participantId) {
        return participantRepository.findById(participantId)
                .map(participant -> {
                    participantRepository.delete(participant);
                    return ResponseEntity.ok().build();
                }).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{participantId}")
    public ResponseEntity<Participant> putParticipant(@PathVariable Long participantId, @Valid @RequestBody Participant participant) {
        return participantRepository.findById(participantId)
                .map(participantDb -> {
                    participantDb.setFirstName(participant.getFirstName());
                    participantDb.setLastName(participant.getLastName());
                    participantDb.setEmail(participant.getEmail());
                    return ResponseEntity.ok(participantRepository.save(participantDb));
                }).orElseGet(() -> ResponseEntity.notFound().build());
    }
}
