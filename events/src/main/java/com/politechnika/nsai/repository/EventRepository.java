package com.politechnika.nsai.repository;

import com.politechnika.nsai.model.Event;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface EventRepository extends MongoRepository<Event, String> {
    Event findByName(String name);

    Optional<Event> findByCode(String code);
}
