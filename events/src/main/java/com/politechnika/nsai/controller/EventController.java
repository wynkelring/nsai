package com.politechnika.nsai.controller;

import com.politechnika.nsai.exception.EventEnrollmentException;
import com.politechnika.nsai.model.Event;
import com.politechnika.nsai.model.ParticipantDto;
import com.politechnika.nsai.repository.EventRepository;
import com.politechnika.nsai.service.EventService;
import com.politechnika.nsai.service.ParticipantServiceClient;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/events")
@RequiredArgsConstructor
public class EventController {

    private final EventRepository eventRepository;
    private final ParticipantServiceClient participantServiceClient;
    private final EventService eventService;

    @GetMapping
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @GetMapping("/{eventCode}")
    public Event getEvent(@PathVariable String eventCode) {
        return eventRepository.findByCode(eventCode).get();
    }

    @PostMapping
    @RolesAllowed("ROLE_ADMIN")
    public Event addEvent(@RequestBody Event event) {
        return eventRepository.save(event);
    }

    @PutMapping("/{eventCode}")
    public Event updateEvent(@RequestBody Event event, @PathVariable String eventCode) {
        Event foundEvent = eventRepository.findByCode(eventCode).get();
        foundEvent.setName(event.getName());
        return  eventRepository.save(foundEvent);
    }

    @GetMapping("/participants")
    public List<ParticipantDto> getAllParticipantsFromParticipantService() {
        return participantServiceClient.getParticipants();
    }

    @GetMapping("/participants/{id}")
    public ParticipantDto getParticipantById(@PathVariable Long id) {
        return participantServiceClient.getParticipantById(id);
    }

    @RolesAllowed("ROLE_USER")
    @PutMapping("/{eventCode}/participants/{participantId}")
    public ResponseEntity<?> addParticipantToEvent(@PathVariable String eventCode, @PathVariable Long participantId) {
        try {
            eventService.addParticipantToEvent(eventCode, participantId);
        } catch (EventEnrollmentException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{eventCode}/participants")
    public List<ParticipantDto> getParticipantsEnrolledOnEvent(@PathVariable String eventCode) throws EventEnrollmentException {
        return eventService.getParticipantEnrolledOnEvent(eventCode);
    }
}
