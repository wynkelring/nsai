package com.politechnika.nsai.exception;

public class EventEnrollmentException extends Exception {

    public EventEnrollmentException(String s) {
        super(s);
    }
}
