package com.politechnika.nsai.service;

import com.politechnika.nsai.exception.EventEnrollmentException;
import com.politechnika.nsai.model.Event;
import com.politechnika.nsai.model.EventParticipant;
import com.politechnika.nsai.model.ParticipantDto;
import com.politechnika.nsai.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang.StringUtils.isBlank;

@Service
@RequiredArgsConstructor
public class EventService {

    private final EventRepository eventRepository;
    private final ParticipantServiceClient participantServiceClient;

    public void addParticipantToEvent(String eventCode, Long participantId) throws EventEnrollmentException {
        Event event = eventRepository.findByCode(eventCode).orElseThrow(() -> new EventEnrollmentException("Event does not exists"));

        ParticipantDto participant = participantServiceClient.getParticipantById(participantId);

        validateParticipant(event, participant);

        event.getEventParticipants().add(new EventParticipant(participant.getEmail(), LocalDateTime.now()));
        eventRepository.save(event);
    }

    private void validateParticipant(Event event, ParticipantDto participant) throws EventEnrollmentException {
        if(isBlank(participant.getEmail())) {
            throw new EventEnrollmentException("Participant email is empty");
        }

        if(event.getEventParticipants().stream().anyMatch(ep -> participant.getEmail().equals(ep.getEmail()))) {
            throw new EventEnrollmentException("Participant is already enrolled to this event");
        }
    }

    public List<ParticipantDto> getParticipantEnrolledOnEvent(String eventCode) throws EventEnrollmentException {
        Event event = eventRepository.findById(eventCode)
                .orElseThrow(() -> new EventEnrollmentException("Event does not exist"));

        List<String> emailParticipant = event.getEventParticipants().stream()
                .map(EventParticipant::getEmail).collect(Collectors.toList());

        return participantServiceClient.getParticipantByEmails(emailParticipant);
    }
}
