package com.politechnika.nsai.service;

import com.politechnika.nsai.model.ParticipantDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "PARTICIPANT-SERVICE")
public interface ParticipantServiceClient {

    @GetMapping("/participants")
    List<ParticipantDto> getParticipants();

    @GetMapping("/participants/email/{email}")
    ParticipantDto getParticipantByEmail(@PathVariable String email);

    @GetMapping("/participants/{id}")
    ParticipantDto getParticipantById(@PathVariable Long id);

    @GetMapping("/participants/emails")
    List<ParticipantDto> getParticipantByEmails(@RequestParam List<String> emails);
}
