package com.politechnika.nsai.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventParticipant {

    private String email;
    private LocalDateTime enrollmentData;
}
