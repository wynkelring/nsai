package com.politechnika.nsai.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ParticipantDto {

    private Long id;

    @NotNull(message = "Nie moze byc puste!")
    private String firstName;

    @NotNull
    @Size(min = 3)
    private String lastName;

    @Email
    private String email;
}
