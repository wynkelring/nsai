package com.politechnika.nsai.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "events_239073")
@Getter
@Setter
@Builder
public class Event {
    @Id
    private String code;

    private String name;
    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime startDate;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime endDate;

    private List<EventParticipant> eventParticipants = new ArrayList<>();


    public List<EventParticipant> getEventParticipants() {
        if (eventParticipants == null) {
            eventParticipants = new ArrayList<>();
        }
        return eventParticipants;
    }
}
