package com.politechnika.nsai;

import com.politechnika.nsai.model.Event;
import com.politechnika.nsai.repository.EventRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
class EventsApplicationTests {

    @Autowired
    EventRepository eventRepository;

    @Test
    void saveEvent() {
        eventRepository.save(Event.builder()
                .code("TE_1")
                .name("Test event")
                .description("Test connetion to db")
                .startDate(LocalDateTime.now())
                .build());
    }

}
