package com.politechnika.nsai;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;

@RestController
public class MainController {

    @GetMapping("/public")
    public String welcomePublic() {
        return "Witaj public";
    }

    @GetMapping("/user")
    @RolesAllowed({"ROLE_USER", "ROLE_ADMIN"})
    public String welcomeUser(Principal principal) {
        return "Witaj " + principal.getName();
    }

    @GetMapping("/admin")
    @RolesAllowed("ROLE_ADMIN")
    public String welcomeAdmin() {
        return "Witaj admin";
    }
}
